# Event Loop

The Event Loop is where all the application code that's inside callback functions is executed.
Some parts will get offloaded to the Thread Pool but it's the Event Loop that takes care of all this.

Node.js is built around callback functions that are being called as soon as some work is finished some time in the future.
It works like this because Node.js uses a Event-driven architecture:

- Events are emitted
- Event loop picks them up
- Callbacks are called

So, that basically means that things like our application receiving an HTTP request, a timer has expired or a file reading has finished 
will all emit events as soon as they are done and the Event Loop will then pick up these events and call the callback functions 
that are associated with each event.

In summary, the event loop does the orchestration which simply means that it receives events, calls their callback functions and 
offloads the more expensive tasks to the Thread Pool.

Event Loop has several phases and each phase has it's own callback queue.
Callbacks in each queue are being processed one by one until there are no callbacks left in the queue 
and only then will the event loop enter the next phase.
These are the four most important phases in order of execution:

1. Expired timer callbacks
2. I/O polling callbacks (networking and file access)
3. setImmediate callbacks (special kind of timer that we can use if we want callbacks to run after I/O)
4. Close callbacks (in this phase all close events are processed for example when a web server or socket shuts down)

Beside these four callback queues there are also two other queues:

- process.nexttick() queue
- other microtasks queue (resolved promises)

If there are any callbacks in one of these two queues they will get processed right after the current phase of the Event Loop finishes 
instead of waiting for the entire loop to finish. In other words, if after each of the mention four phases if there are any callbacks 
in these two special queues they will be executed right away.

After the complete cycle has finished Node.js determines whether the Event Loop should continue to the next cycle or the process should exit.
How Node.js does this is simply by checking if there are any timers or I/O tasks that are still running in the background and if there aren't any then it 
will simply exit the application.
So for example, when we are listening for incoming HTTP requests we are basically running an I/O task 
and that is why Node.js keeps running instead of just exiting the application.

## Summary

Event Loop is what makes asynchronous programming possible in Node.js making it the most important feature in Node.js design 
and making Node.js completely different from other platforms. 
It takes care of all incoming events and performs orchestration by offloading heavy tasks into the Thread Pool 
and doing the more simple tasks itself. 
