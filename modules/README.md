# Modules

In a Node.js module system each JavaScript file is treated as a separate module. 
Node.js uses the CommonJS module system: ```require()```, ```exports``` or ```module.exports```.

Now, you might be wondering why in Node.js each and every module actually gets access to the ```require()``` function 
in order to import modules in the first place because it's not a standard JavaScript function?
Where does it come from? And how exactly does it work behind the scenes?  
As a very broad overview, the following steps are executed behind the scenes:

1. First, the path to the required module is resolved and the file is loaded 
2. then a process called wrapping happens
3. after that the module code is executed
4. the module exports are returned
5. and finally the entire module gets cached.

## Let's now look at each step in more detail.  

First of, how does node know which file to load when we require a module? 
Because remember, we can actually load three different kind of modules:

- Core modules (```require('http');```)
- Developer modules (```require('./lib/controller');```)
- 3rd-party modules (```require('express');```)

This process is known as resolving the file path and this is how it works:

- Start with core modules
- if begins with ```./``` or ```../``` try to load developer module
- if no file found try to find folder with ```index.js``` in it
- else, go to ```node_modules/``` and try to find node modules there.

Moving on, after the module is loaded, the module's code is wrapped into a special function 
which will give us access to a couple of special objects.

```
(function(exports, require, module, __filename, __dirname) {
    // Module code lives here...
});
```

This step is where the magic happens and it is here where we get the answer to the question where does the ```require``` function 
actually come from and why do we have access to it. 
It's because the Node.js runtime takes the code of our module and puts it inside the immediately invoked function expression 
,or IIFE, that you can see above. 
So Node.js does not execute the file code directly but instead the wrapper function that will contain our code in it's body.
It also passes the ```exports```, ```require```, ```module```, ```__filename``` and ```__dirname``` props into it. 
That is why in every module we automatically have access to stuff like the ```require``` function and these are basically like 
global variables that are injected into each and every module.  
By doing this, node achieves two very important things. 
First, of course, is giving developers access to all these variables we just talked about and second, it keeps the top level 
variables that we define in our modules private so it is scoped only to the current module instead of leaking everything into 
the global object.  
Let's now take a quick look at each object that our module gets:

1. ```require```: function to require modules
2. ```module```: reference to the current module
3. ```exports```: a reference to ```module.exports``` used to export object from a module
4. ```__filename```: absolute path of the current module's file
5. ```__dirname```: directory name of the current module

And that is how the wrapping step of loading a module works.

Next up, the code in the module actually gets executed by the Node.js runtime and now it's time for the ```require()``` 
function to actually return something and what it returns is the exports of the required module. 
These exports are stored in the ```module.exports``` object.

Here is what you need to know about when to use ```module.export``` or just ```export```. 

- If all you want to do is to export one single variable, like one class or one function, you usually use ```module.exports``` 
and set it equal to the variable that you want to export. (```module.exports = Calculator```)
- On the other hand, if you're looking to export multiple named variables like multiple functions for example, then you 
should create these as properties of the export object. (```exports.add = (a, b) => a + b```)

The last step is that the modules are actually cached after the first time they are loaded and what this means is that if 
you require the same module multiple times, you will always get the same result and the code in the modules is actually 
only executed in the first call. In subsequent calls the result is simply retrieved from cache.
