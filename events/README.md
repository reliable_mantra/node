# Events

Most of Node.js modules like ```http```, ```fs``` (file system) and ```timers``` are built around an event driven architecture.
We can also use this architecture to our advantage in our code and the concept is actually quite simple.

In Node.js there are certain objects called Event Emitters (instance of EventEmitter class) that emit named events 
as soon as something important happens in the application, like a request hitting the server or a timer expiring 
or a file finishing reading.
These events can than be picked up by event listeners that we developers set up which will fire up callback functions 
that are attached to each listener.

This event emitter logic is called the Observer Pattern in JavaScript programming in general and it's quite a popular pattern 
with many use cases.  
So the idea is that there is an observer, in this case event listener, which keeps waiting (observing) the subject that will 
eventually emit an event that the listener is waiting for.  
The opposite of this pattern is simply functions calling other functions and observer pattern has been designed to react 
rather then call, and that is because there is a huge benefit of using this architecture which is the fact that everything 
is more decoupled.
We don't have, for example, functions from the file system module calling functions from the http module because it would be a 
huge mess, instead these modules are nicely decoupled and self-contained each emitting events that other functions, 
even if they come from other modules, can respond to.  
Also, using the event driven architecture makes it way more straight forward to react multiple times to the same event,
all we have to do is to set up multiple listeners.
