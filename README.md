# Node.js

Node run time has several dependencies and the most important ones are googles V8 JavaScript Engine and libuv.
V8 is what converts JavaScript code into machine code that a computer can actually understand, but that alone is not enough 
to create a whole server side framework like Node.js and that is why we also have libuv which is an open source library with 
a strong focus on async I/O. 

So libuv layer is what gives Node.js access to the underlying operating system, file system, networking... and more. Besides that, 
libuv also implements two extremely important features of Node.js which are the Event Loop and the Thread Pool.

In simple terms, Event Loop is responsible for handling simple tasks like executing callbacks and network I/O, 
while the Thread Pool is for more heavy work like file access, file compression or something like that.

One important thing to note is that libuv is actually completely written in C++ and not in JavaScript and V8 also uses C++ besides JavaScript.  
This architecture allows us to write 100% pure JavaScript code running in Node.js and still access functions like file reading 
which behind the scenes is actually written in libuv or other libraries in C++ language.

Node.js also relies on http-parser, c-ares for some DNS request stuff, OpenSLL for cryptography and zlib for compression.

## Node.js Process

When we use Node.js on a computer it means that there is a node.js process running on that computer, and a process is just a program in execution. 
This is important because in Node.js we actually have access to a global ```process``` object that provides information about, 
and control over, the current Node.js process.

Now in that process Node.js runs in a so called single thread, which makes it easy to block Node.js applications.
This is important to know because this is one of the unique features that Node.js brings to the table, and no matter if you have 
ten users or ten million users accessing your application at the same time your Node.js application will still run on that single thread.
This is why you need to be very careful about not blocking that single thread.

In other languages like PHP running on an Apache server a new thread is created for each new request which is way more resource intensive 
but on the other hand there is no danger of blocking.

When a Node.js application is initialized all the top-level code (any code that is not within a callback function) is executed, 
also all the modules that the app needs are required and all callbacks are registered. 
Then after all that the Event Loop finally starts running and this is where most of the work is done in your app, 
so it's really the hart of the whole Node.js architecture.

Now, some tasks are actually too heavy, too expensive, to be executed in the Event Loop because they would then block the single thread, 
and this is where the Thread Pool comes in which just like the Event Loop is provided to Node.js by the libuv library.

## Thread Pool

The Thread Pool gives us 4 additional threads (this can be configured up to 128 threads) that are completely separate from the main single thread.
Event Loop automatically offloads heavy tasks to the Thread Pool and all this happens behind the scenes, so it's not us 
developers who decide what goes to the Thread Pool and what doesn't.
The expensive tasks that do get offloaded are all operations dealing with:

- File system APIs
- Cryptography
- Compression
- DNS lookups
