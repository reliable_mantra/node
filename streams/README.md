# Streams

Streams are yet another fundamental concept in Node.js. With streams, we can process (read and write) data piece by piece (chunks), 
without completing the whole read or write operation, and therefore without keeping all the data in memory.
For example when we read a file using streams we read part of the data, do something with it, free our memory and repeat this 
until the entire file has been processed.

Companies like Netflix and YouTube are called streaming companies because they stream videos using the same principle.
So, instead of waiting until the entire video loads the processing is done piece by piece, or in chunks, so that you can start 
watching even before the entire file has been downloaded. 
So the streams principle is not unique to Node.js but universal to computer since in general.

This makes streams perfect for handling large volumes of data and it also makes the data processing more efficient in terms of 
memory (no need to keep all data in memory) and time (we don't have to wait until all the data is available).

## Fundamentals

In Node.js there are 4 fundamental types of streams:

- Readable streams
- Writable streams
- Duplex streams
- Transform streams

Streams are actually an instance of the EventEmitter class, meaning that all streams can emit and listen to named events.  
You are almost always going to use existing Node.js streams so you will probably never going to be creating your own streams.

## Readable streams

Readable streams are the ones from which we can read (consume) data.  
For example the data that comes in when the ```http``` server gets a request is actualy a readable stream and all the data that is sent 
with the request comes in piece by piece and not in one large piece.  
Also, another example but from the file system is that we can read files piece by piece using the read stream from the ```fs``` module 
which can actually be quite useful for large text files.

In case of readable streams we can emit, and we can listen to many different events but the most important two 
are the ```data``` and the ```end``` event.
The ```data``` event is emitted when there is a new piece of data to consume and the ```end``` event is emitted as soon 
as there's no more data to consume.

The most important functions we can use on readable streams are ```pipe()``` and ```read()```. 
The ```pipe()``` function allows us to pipe streams together passing data from one stream to another without having to worry 
much about events at all.

## Writable streams

Writable streams are the ones to which we can write data, so basically the opposite of readable streams.  
A great example is the ```http``` response that we can send back to the client and which is actually a writable stream, so a stream 
that we can write data into. 
When we want to send data we have to write it somewhere and that somewhere is a writable stream, for example if we wanted to send 
a big video file to a client we would stream that result just like Netflix or YouTube do.

The most important events ```drain``` and the ```finish``` events.

The most important functions are the ```write()``` and ```end()``` functions.

## Duplex streams

Duplex streams are both readable and writable at the same time.  
These are a bit less common but a good example would be a web socket from the ```net``` module.
Web socket is basically just a communication channel between client and server that works in both directions and stays open 
once the connection has been established.

## Transform streams

Transform streams are duplex streams which at the same time can modify or transform the data as it is read or written.  
A good example of this one is the ```zlib``` core module for compressing data which actually uses a transform stream.

